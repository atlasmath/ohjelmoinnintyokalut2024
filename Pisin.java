public class Pisin {
    public static void main(String[] args) {
        String pisin = "";

        for (String r : args) {
            if (pisin.length() < r.length()) {
                pisin = r;
            }
        }
        if (args.length != 0) {
            System.out.println("Pisin parametri: " + pisin);
        } else {
            System.out.println("Ei parametreja");
        }
    }
}
